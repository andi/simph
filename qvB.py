#!/usr/bin/env python3
#coding:utf-8

import svgwrite
from svgwrite import cm, mm
from math import sqrt

SCRIPT = """
function B_up(evt){
  var B = +document.getElementById('B').innerHTML + +0.05;
  document.getElementById('B').innerHTML = B.toFixed(2);
  updateAll();
}

function B_dn(evt){
  var B = +document.getElementById('B').textContent - +0.05;
  if ( B > 0 ){
    document.getElementById('B').textContent = B.toFixed(2);
    updateAll();
  }
}

function U_up(evt){
  var U = +document.getElementById('U').textContent + +5;
  document.getElementById('U').textContent = U.toFixed(0);
  updateAll();
}

function U_dn(evt){
  var U = +document.getElementById('U').textContent - +5;
  if ( U > 0 ){
    document.getElementById('U').textContent = U.toFixed(0);
    updateAll();
  }
}

function updateAll(){
  var B = +document.getElementById('B').textContent;
  var U = +document.getElementById('U').textContent;
  var R = +1e6/B * Math.sqrt(2*U/1.75882e11);
  var circle = document.getElementById('circle');
  circle.setAttribute("r", R  + 'mm');
  circle.setAttribute("cy", +R + +5 + 'mm');
  circle.setAttribute("stroke-width", U/100);
  var center = document.getElementById('center');
  center.setAttribute("cy", +R + +5 + 'mm');
  var radius = document.getElementById('radius');
  radius.setAttribute("y1", +R + +5 + 'mm');
  radius.setAttribute("x2", 0.55*R + 'mm');
  radius.setAttribute("y2", 5+R-R*Math.sqrt(1-0.55**2) + 'mm');
  document.getElementById('R').textContent = R.toFixed(1) + ' mm';
  document.getElementById('rectangle').setAttribute("opacity", B*0.5);
  if (B > 0.7){
    document.getElementById('vector').setAttribute("stroke", "white");
  }else{
    document.getElementById('vector').setAttribute("stroke", "red");
  }
}
"""

B = 0.55                        # in mT
U = 200                         # in V
r = 1e6/B*sqrt(2*U/1.75882e11)  # in mm

def qvB(name):
    dwg = svgwrite.Drawing(filename=name, size=('10cm','13.5cm'), debug=True, onload="updateAll()")

    clip_path = dwg.defs.add(dwg.clipPath(id='field'))
    clip_path.add(dwg.rect(size=(10*cm, 10*cm)))

    rectangle = dwg.rect(size=(10*cm, 10*cm), fill='red', stroke='red', \
                    id='rectangle', opacity=B*0.5)
    box = dwg.rect(size=(10*cm, 13.5*cm), fill='none', stroke='gray', stroke_width=2)

    dwg.add(rectangle)
    dwg.add(box)

    vector = dwg.add(dwg.g(id='vector', stroke='red', fill='none', stroke_width=4))
    vector.add(dwg.circle(center=(5*cm, 5*cm), r=1.4*cm))
    vector.add(dwg.line(start=(4*cm,4*cm), end=(6*cm,6*cm)))
    vector.add(dwg.line(start=(4*cm,6*cm), end=(6*cm,4*cm)))

    x = 270
    y = 420
    B_button = dwg.add(dwg.g(id='B_button', fill='red'))
    B_button.add(dwg.text("B =", insert=(x-170, y+5), font_size=20))
    B_button.add(dwg.text("mT", insert=(x-55, y+5), font_size=20))
    B_button.add(dwg.text(B, insert=(x-115,y+5), font_size=20, id='B'))
    B_button.add(dwg.polygon([(x+0,y-3), (x+30,y-3), (x+15,y-3-15)], onclick = "B_up(evt)"))
    B_button.add(dwg.polygon([(x+0,y+3), (x+30,y+3), (x+15,y+3+15)], onclick = "B_dn(evt)"))

    x = 270
    y = 470
    U_button = dwg.add(dwg.g(id='U_button', fill='blue'))
    U_button.add(dwg.text("U =", insert=(x-170, y+5), font_size=20))
    U_button.add(dwg.text("V", insert=(x-55, y+5), font_size=20))
    U_button.add(dwg.text(U, insert=(x-115,y+5), font_size=20, id='U'))
    U_button.add(dwg.polygon([(x+0,y-3), (x+30,y-3), (x+15,y-3-15)], onclick = "U_up(evt)"))
    U_button.add(dwg.polygon([(x+0,y+3), (x+30,y+3), (x+15,y+3+15)], onclick = "U_dn(evt)"))

    x = 270
    y = 20
    R_show = dwg.add(dwg.g(id='R_show', fill='black'))
    R_show.add(dwg.text("r =", insert=(x-120, y+5), font_size=15))
    R_show.add(dwg.text(text='%.1f mm' % r, insert=(x-85,y+5), font_size=15, id='R'))

    circle = dwg.circle(center=(0*mm, (5+r)*mm), r=r*mm, fill='none', id='circle', \
                        clip_path="url(#field)", stroke='blue', stroke_width=U/100, \
                        stroke_dasharray=([5, 5]))

    rad = dwg.line(start=(0*mm,(5+r)*mm), end=((0.55*r)*mm,(5+r-r*sqrt(1-0.55**2))*mm), \
                   stroke_width=1, stroke='black', id='radius', clip_path="url(#field)")
    center = dwg.circle(center=(0*mm,(5+r)*mm), r=1.5*mm, id='center', clip_path="url(#field)")

    dwg.add(rad)
    dwg.add(center)
    dwg.add(circle)
    dwg.defs.add(dwg.script(content=SCRIPT))

    dwg.save()


if __name__ == '__main__':
    qvB('qvB.svg')
